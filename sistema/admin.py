from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Ciudad)
admin.site.register(Evento)
admin.site.register(Horario)
admin.site.register(Institucion)
admin.site.register(Persona)
admin.site.register(Organizador)
admin.site.register(Organiza)
admin.site.register(Asistencia)
admin.site.register(RegistroAsistencia)
admin.site.register(Ponencia)