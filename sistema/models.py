from __future__ import unicode_literals
from datetime import *
from django.contrib.auth.models import User 
from django.db import models

# Create your models here.
niveles = (('Primaria','Primaria'),
	('Secundaria','Secundaria'),
	('Tercer','Tercer'),
	('Cuarto','Cuarto'),
	('Quinto','Quinto'))

generos = (('Femenino','Femenino'),
	('Masculino','Masculino'))

profesion = (('0','No'),
	('1','Si'))

class Ciudad(models.Model):
	nombre = models.CharField(max_length=50)
	provincia = models.CharField(max_length=50)
	pais = models.CharField(max_length=50)

	def __unicode__(self):
		return self.nombre + " - " + self.provincia

class Evento(models.Model):
	codigo = models.CharField(max_length=15)
	nombre = models.CharField(max_length=50)
	inicio = models.DateField()
	fin = models.DateField()
	duracion = models.IntegerField()
	horas = models.IntegerField()
	lugar = models.CharField(max_length=50)
	descripcion = models.TextField()
	precio_estudiante = models.DecimalField(decimal_places=2,max_digits=6)
	precio_externo = models.DecimalField(decimal_places=2,max_digits=6)
	precio_profesional = models.DecimalField(decimal_places=2,max_digits=6)
	imagen = models.ImageField(upload_to='fondo/')

	def __unicode__(self):
		return self.codigo + " " +  self.nombre

	def fecha_fin(self):
		self.fin = self.inicio + timedelta(days=(self.duracion-1))

	def validar_fechas(self):
		if self.inicio>self.fin and self.inicio>datetime.today().time():
			return False
		return True

	def validar_horario(self,horario):
		retorno=True
		query=Horario.objects.filter(evento=self)
		horarios=list(query)
		print "Horarios: ", horarios
		for elemento in horarios:
			print "Elemento: ",elemento
			print "dia nuevo: ",horario.dia,", dia anterior: ", elemento.dia
			if horario.dia==elemento.dia:
				print "igual dia"
				if (horario.horaInicio>=elemento.horaInicio and horario.horaFin<=elemento.horaFin) or (horario.horaInicio<=elemento.horaInicio and horario.horaFin>=elemento.horaFin) or (horario.horaInicio<elemento.horaInicio and horario.horaFin>elemento.horaInicio) or (horario.horaInicio<elemento.horaFin and horario.horaFin>elemento.horaFin):
					print "choca con el horario", elemento
					retorno=False
		return retorno

	def validar_dia(self,elemento):
		dia = elemento.dia
		inicio = self.inicio
		fin = self.fin
		print "dia horario: ", dia
		print "Inicio evento: ",inicio
		print "Fin evento: ", fin
		if dia>=inicio and dia<=fin:
			print "dia valido"
			return True
		return False

class Horario(models.Model):
	dia = models.DateField()
	horaInicio = models.TimeField()
	horaFin = models.TimeField()
	evento = models.ForeignKey(Evento,blank=True)
	tomado = models.BooleanField(default = False)
	horas = models.IntegerField(default = 0)

	def __unicode__(self):
		return str(self.id) + " Dia:" + str(self.dia) + " Inicio:" + str(self.horaInicio) + " Fin:" + str(self.horaFin)

	def validar_horas(self):
		if self.horaInicio>self.horaFin:
			return False
		print "horas validas"
		return True
	
class Institucion(models.Model):
	nombre = models.CharField(max_length=50)
	ciudad = models.ForeignKey(Ciudad)

	def __unicode__(self):
		return self.nombre

class Persona(models.Model):
	cedula = models.CharField(max_length=15,primary_key=True)
	nombres = models.CharField(max_length=50)
	apellidos = models.CharField(max_length=50)
	correo = models.EmailField(max_length=50)
	direccion = models.CharField(max_length=50)
	genero = models.CharField(max_length=10, choices = generos)
	profesional = models.CharField(max_length=10, choices = profesion)
	institucion = models.ForeignKey(Institucion)
	ciudad = models.ForeignKey(Ciudad)
	imagen = models.ImageField(upload_to='perfil/')
	admin = models.BooleanField(default = False)
	activo = models.BooleanField(default = False)
	usuario = models.OneToOneField(User, unique=True)

	def __unicode__(self):
		return self.nombres + " " + self.apellidos + " " + self.cedula

class Organizador(models.Model):
	cedula = models.CharField(max_length=15,primary_key=True)
	nombres = models.CharField(max_length=50)
	apellidos = models.CharField(max_length=50)
	cargo = models.CharField(max_length=50)
	titulo = models.CharField(max_length=50)
	nivel = models.CharField(max_length=30)
	firma = models.ImageField(upload_to='firmas/')

	def __unicode__(self):
		return self.nombres + " " + self.apellidos + " " + self.cedula

class Organiza(models.Model):
	evento = models.ForeignKey(Evento)
	organizador = models.ForeignKey(Organizador)
	responsabilidad = models.CharField(max_length=50)

	def __unicode__(self):
		return self.evento.nombre + " " + self.organizador

class Asistencia(models.Model):
	asistente = models.ForeignKey(Persona)
	evento = models.ForeignKey(Evento)
	porcentaje = models.DecimalField(decimal_places=2,max_digits=5,default = 0)
	comprobante = models.CharField(max_length=30,default="0")
	universidad = models.CharField(max_length=50)
	nivel = models.CharField(max_length=30, choices = niveles)
	profesion = models.CharField(max_length=50)

	def __unicode__(self):
		return self.evento.nombre + " " + self.asistente.__unicode__()

class RegistroAsistencia(models.Model):
	asistencia = models.ForeignKey(Asistencia)
	horario = models.ForeignKey(Horario)
	asistio = models.BooleanField(default = False)
	horas = models.IntegerField()

	def __unicode__(self):
		return self.asistencia.asistente.apellidos + " " + self.asistencia.asistente.nombres + " " + self.asistencia.asistente.cedula

	def set_horas(self):
         self.horas = self.horario.horas

class Ponencia(models.Model):
	topico = models.CharField(max_length=50)
	dia = models.DateField()
	hora = models.TimeField()
	ponente = models.CharField(max_length=50)
	evento = models.ForeignKey(Evento,blank=True)

	def __unicode__(self):
		return self.topico + " " + self.ponente + " " + self.evento.nombre
