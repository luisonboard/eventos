#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.db import models as model
from django.contrib.auth.models import User as user
from .models import *

class CiudadForm(forms.ModelForm):
        nombre = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Ciudad', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        provincia = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Provincia', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        pais = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Pais', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
		
        class Meta:
            model = Ciudad
            fields = ('nombre', 'provincia','pais')

class EstudianteForm(forms.ModelForm):
        genero = forms.ChoiceField(choices=generos,
                                   widget=forms.Select(attrs={'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        cedula = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Cedula', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        nombres = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Nombre', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        apellidos = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Apellido', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        direccion = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Direccion', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        profesional = forms.ChoiceField(choices=profesion,
                                   widget=forms.Select(attrs={'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))

        institucion = forms.ModelChoiceField(queryset=Institucion.objects.all(), empty_label='Seleccione una Institucion',
                                             widget=forms.Select(attrs={'id': 'form-field-select-3',
                                                                        'class': 'chosen-select form-control'}))
        ciudad = forms.ModelChoiceField(queryset=Ciudad.objects.all(), empty_label='Seleccione una Ciudad',
                                        widget=forms.Select(
                                            attrs={'id': 'form-field-select-3', 'class': 'chosen-select form-control'}))
        
        imagen = forms.ImageField(widget=forms.FileInput(attrs={'type': 'file', 'id': 'cargarFirma'}))


        class Meta:
			model = Persona
			fields = ('cedula', 'nombres','apellidos','direccion','genero','profesional','institucion','ciudad', 'imagen')

class EventoForm(forms.ModelForm):
        duracion = forms.IntegerField(widget=forms.TextInput(attrs={'id': 'duracion', 'class': 'form-control'}))
        horas = forms.IntegerField(widget=forms.TextInput(attrs={'id': 'horas', 'class': 'form-control'}))
        precio_estudiante = forms.DecimalField(widget=forms.TextInput(attrs={'id': 'precio_est', 'class': 'form-control'}))
        precio_profesional = forms.DecimalField(widget=forms.TextInput(attrs={'id': 'precio_pro', 'class': 'form-control'}))
        precio_externo = forms.DecimalField(widget=forms.TextInput(attrs={'id': 'precio_ext', 'class': 'form-control'}))
        codigo = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Codigo', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        lugar = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Lugar del Evento', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        nombre = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Evento', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        inicio = forms.DateField(widget=forms.DateInput(
            attrs={'placeholder': 'Escoga una Fecha', 'class': 'form-control date-picker',
                   'data-date-format': 'yyyy-mm-dd'}))
        descripcion=forms.CharField(
            widget=forms.Textarea(attrs={'placeholder': 'Descripcion del Evento','class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        imagen = forms.ImageField(widget=forms.FileInput(attrs={'type': 'file', 'id': 'cargarFirma'}))

        class Meta:
            model = Evento
            fields = ('codigo', 'nombre','inicio','duracion','horas','lugar','precio_estudiante','precio_externo','precio_profesional','descripcion','imagen')

class InstitucionForm(forms.ModelForm):
        nombre = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Nombre de Institucion', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        ciudad = forms.ModelChoiceField(queryset=Ciudad.objects.all(), empty_label='Seleccione una Ciudad',
                                        widget=forms.Select(
                                            attrs={'id': 'form-field-select-3', 'class': 'chosen-select form-control'}))
        class Meta:
            model = Institucion
            fields = ('nombre','ciudad')

class OrganizadorForm(forms.ModelForm):
        nombres = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Nombres', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        apellidos = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Apellidos', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        cedula = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Cedula', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        titulo = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Titulo', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        cargo = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Cargo', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        nivel = forms.ChoiceField(choices=niveles,
                                  widget=forms.Select(attrs={'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))

        firma = forms.ImageField(widget=forms.FileInput(attrs={'type': 'file', 'id': 'cargarFirma'}))
        class Meta:
            model = Organizador
            fields = ('cedula', 'nombres','apellidos','cargo','titulo','nivel','firma')

class OrganizaForm(forms.ModelForm):
        responsabilidad = forms.CharField(widget=forms.TextInput(
            attrs={'placeholder': 'Responsabilidad', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        evento = forms.ModelChoiceField(queryset=Evento.objects.all(), empty_label='Seleccione un Evento',
                                        widget=forms.Select(
                                            attrs={'id': 'form-field-select-3', 'class': 'chosen-select form-control'}))
        organizador = forms.ModelChoiceField(queryset=Organizador.objects.all(), empty_label='Seleccione un Organizador',
                                        widget=forms.Select(
                                            attrs={'id': 'form-field-select-3', 'class': 'chosen-select form-control'}))

        class Meta:
            model = Organiza
            fields = ('evento', 'organizador', 'responsabilidad')

class formLog(forms.ModelForm):
	# create your own error message key & value

    error_messages = {
        'duplicate_username': 'Los datos no coinciden con ningun usuario'
    }
    password = forms.CharField(label='Contraseña',widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña', 'class':'form-control'}))
    
    username = forms.CharField(label='Usuario',widget=forms.TextInput(attrs={'placeholder': 'Username', 'class':'form-control'}))
    
    class Meta:
		model=user
		error_messages = {
            'username': {
				'max_length': "This writer's name is too long.",
			},
        }

        
		labels = {
			'username': None, 
			'password':None
			
		}
		
		help_texts = {
            'username': None,
        }
		
		fields=['username','password']
		
		#email = forms.EmailField(label=_("E-mail"),
                 #    widget=forms.TextInput(attrs={'placeholder': 'Your Email', 'class':'your_css_code'}))
		
    
    # override the clean_<fieldname> method to validate the field yourself

    def clean_username(self):
        username = self.cleaned_data["username"]
       
        try:
            user._default_manager.get(username=username)
            #if the user exists, then let's raise an error message
			
            raise forms.ValidationError( 
              self.error_messages['duplicate_username'],  #user my customized error message

              code='duplicate_username',   #set the error message key

                )
        except user.DoesNotExist:
			raise forms.ValidationError( 
              self.error_messages['duplicate_username'],  #user my customized error message

              code='duplicate_username',   #set the error message key

                )
			return username # great, this user does not exist so we can continue the registration process

class newUser(forms.ModelForm):
	# create your own error message key & value

    error_messages = {
        'duplicate_username': 'El usuario ya existe'
    }
    email = forms.EmailField(label="E-mail", widget=forms.TextInput(attrs={'placeholder': 'Email', 'class':'form-control'}))
    
    password = forms.CharField(label='Contraseña',widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña', 'class':'form-control'}))
    repassword = forms.CharField(label='Repita Contraseña',widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña', 'class':'form-control'}))
    username = forms.CharField(label='Usuario',widget=forms.TextInput(attrs={'placeholder': 'Username', 'class':'form-control'}))
    
    class Meta:
		model=user
		error_messages = {
            'username': {
				'max_length': "This writer's name is too long.",
			},
        }

		help_texts = {
            'username': None,
        }
		
		fields=['username','email','password','repassword']
		
		#email = forms.EmailField(label=_("E-mail"),
                 #    widget=forms.TextInput(attrs={'placeholder': 'Your Email', 'class':'your_css_code'}))
		
    
    # override the clean_<fieldname> method to validate the field yourself

    def clean_username(self):
        username = self.cleaned_data["username"]
       
        try:
            user._default_manager.get(username=username)
            #if the user exists, then let's raise an error message
			
            raise forms.ValidationError( 
				
              self.error_messages['duplicate_username'],  #user my customized error message

              code='duplicate_username',   #set the error message key
              

                )
        except user.DoesNotExist:
			
			return username # great, this user does not exist so we can continue the registration process

class email(forms.ModelForm):
	retemail = forms.EmailField(label="E-mail", widget=forms.TextInput(attrs={'placeholder': 'Email', 'class':'form-control'}))
	class Meta:
		model=user
		fields=['retemail']

class ValidarAsistenciaForm(forms.ModelForm):
        
        comprobante = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Nro Comprobante', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        
        class Meta:
            model = Asistencia
            fields = ('comprobante',)

class PonenciaForm(forms.ModelForm):
        topico = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Tópico', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        hora = forms.TimeField(widget=forms.TimeInput(format='%H:%M',attrs={'placeholder': 'HH:MM', 'class':'timepicker','id':'timepicker1'}))
        ponente = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder': 'Ponente', 'class': 'form-control', 'class': 'col-xs-10 col-sm-5'}))
        
        dia = forms.DateField(widget=forms.DateInput(
            attrs={'placeholder': 'Escoga una Fecha', 'class': 'form-control date-picker',
                   'data-date-format': 'yyyy-mm-dd'}))
        class Meta:
            model = Ponencia
            fields = ('topico', 'hora','ponente','dia')

class HorarioForm(forms.ModelForm):
        horaInicio = forms.TimeField(widget=forms.TimeInput(format='%H:%M',attrs={'placeholder': 'HH:MM', 'class':'timepicker','id':'timepicker1'}))

        horaFin = forms.TimeField(widget=forms.TimeInput(format='%H:%M',attrs={'placeholder': 'HH:MM', 'class':'timepicker','id':'timepicker2'}))

        horas = forms.IntegerField(widget=forms.TextInput(attrs={'id': 'duracion', 'class': 'form-control'}))

        dia = forms.DateField(widget=forms.DateInput(
            attrs={'placeholder': 'Escoga una Fecha', 'class': 'form-control date-picker',
                   'data-date-format': 'yyyy-mm-dd'}))

        class Meta:
            model = Horario
            fields = ('horaInicio','horaFin','dia','horas')

class TomarAsistenciaForm(forms.Form):
    def __init__(self,*args,**kwargs):
        queryset = kwargs.pop('queryset')
        print "query form", queryset
        lleno = kwargs.pop('lleno')
        super(TomarAsistenciaForm,self).__init__(*args,**kwargs)
        # Set queryset from argument.
        if not lleno:
            self.fields['registrosasistencias'].queryset = queryset

    # Set choices to an empty list as it is a required argument.
    registrosasistencias=forms.ModelMultipleChoiceField(queryset=RegistroAsistencia.objects.all(),widget=forms.CheckboxSelectMultiple())

    def save(self):
        data = self.cleaned_data
        print "limpio"
        query = data['registrosasistencias']
        print query
        for registroasistencia in query:
            registroasistencia.asistio = True
            print registroasistencia
            registroasistencia.save()