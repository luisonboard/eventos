from django.conf.urls import include, url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
	url(r'^$', views.login),
    url(r'^index/$', views.base, name='index'),
    url(r'^ciudad/nueva/$', views.ciudad_nueva, name='ciudad_nueva'),
    url(r'^estudiante/nuevo$', views.estudiante_nuevo, name='estudiante_nuevo'),
    url(r'^evento/nuevo$', views.evento_nuevo, name='evento_nuevo'),
    url(r'^institucion/nuevo$', views.institucion_nuevo, name='institucion_nuevo'),
    url(r'^organizador/nuevo$', views.organizador_nuevo, name='organizador_nuevo'),
    url(r'^organiza/nuevo$', views.organiza_nuevo, name='organiza_nuevo'),
    url(r'^logout$', views.logout),
    url(r'^lista_ciudades$', views.lista_ciudades,name='listaCiudad'),
    url(r'^lista_organizar$', views.lista_organizar, name='lista_organizar'),
    url(r'^lista_estudiantes$', views.lista_estudiantes,name='lista_estudiantes'),
    url(r'^lista_instituciones$', views.lista_instituciones,name='lista_instituciones'),
    url(r'^lista_eventos$', views.lista_eventos,name='lista_eventos'),
    url(r'^lista_organizador$', views.lista_organizador,name='lista_organizador'),
    url(r'^editarCiudad/(?P<id>[0-9]+)$', views.editar_ciudad, name='editar_ciudad'),
    url(r'^editarEstudiante/(?P<cedula>[0-9]+)$', views.editar_estudiante, name='editar_estudiante'),
    url(r'^editarInstitucion/(?P<id>[0-9]+)$', views.editar_institucion, name='editar_institucion'),
    url(r'^editarEvento/(?P<codigo>.*)$', views.editar_evento, name='editar_evento'),
    url(r'^editarOrganizador/(?P<cedula>[0-9]+)$', views.editar_organizador, name='editar_organizador'),
    url(r'^editar_organiza/(?P<id>[0-9]+)$', views.editar_organiza, name='editar_organiza'),
    url(r'^perfilUsuario/(?P<cedula>[0-9]+)$', views.perfilUsuario, name='perfilUsuario'),
    url(r'^baseUsuario/$', views.baseUsuario, name='baseUsuario'),
    url(r'^indexUsuario/$', views.indexUsuario, name='indexUsuario'),
    url(r'^datoEstudiante/$', views.datoEstudiante, name='datoEstudiante'),
    url(r'^editar_perfil/(?P<cedula>[0-9]+)$', views.editar_perfil, name='editar_perfil'),
    url(r'^listaEventosU/$', views.listaEventosU, name='listaEventosU'),
    url(r'^listaEventosAsistidos/$', views.listaEventosAsistidos, name='listaEventosAsistidos'),
    url(r'^listaEventosPorAsistir/$', views.listaEventosPorAsistir, name='listaEventosPorAsistir'),
    url(r'^validar_asistencia/(?P<codigo>[0-9]+)$', views.validar_asistencia, name='validar_asistencia'),
    url(r'^horario/nuevo/(?P<codigoevento>.*)$', views.horario_nuevo, name='horario_nuevo'),
    url(r'^imprimirEvento/(?P<codigo>[a-zA-Z0-9_]+)$', views.imprimirEvento, name='imprimirEvento'),
    url(r'^ordenesPago', views.ordenesPago, name='ordenesPago'),
    url(r'^template/(?P<codigo>[a-zA-Z0-9_]+)$', views.template, name='template'),
    url(r'^ponencia/nuevo/(?P<codigoevento>.*)$', views.ponencia_nuevo, name='ponencia_nuevo'),
    url(r'^verTopico/(?P<codigo>[a-zA-Z0-9_]+)$', views.verTopico, name='verTopico'),
    url(r'^lista_ponencias$', views.lista_ponencias, name='lista_ponencias'),
    url(r'^editar_ponencias/(?P<id>[0-9]+)$', views.editar_ponencias, name='editar_ponencias'),
    url(r'^listaValidar', views.listaValidar, name='listaValidar'),
    url(r'^lista_horario/$', views.lista_horario, name='lista_horario'),
    url(r'^editar_horario/(?P<id>[0-9]+)$', views.editar_horario, name='editar_horario'),
    url(r'^tomar_asistencia/(?P<idhorario>[0-9]+)$', views.tomar_asistencia, name='tomar_asistencia'),
    url(r'^listaCertificados', views.listaCertificados, name='listaCertificados'),
    url(r'^listaHorariosPorRegistrar/$', views.listaHorariosPorRegistrar, name='listaHorariosPorRegistrar'),
	url(r'^imprimirCertificado/(?P<codigo>[0-9]+)$', views.imprimirCertificado, name='imprimirCertificado'),
	url(r'^generarCertificado/(?P<codigo>[0-9]+)$', views.generarCertificado, name='generarCertificado'),
	
	



]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


