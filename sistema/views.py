#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from .forms import *
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_log
from django.contrib.auth import logout as auth_logout
from django.contrib import auth
from django.shortcuts import redirect
from forms import formLog
from django.contrib.auth.models import User as user
from django.http import HttpResponse
from django.core.mail import EmailMessage
from random import choice
from .models import *
import datetime as fecha
from django.template.loader import get_template
from django.template import Context
import pdfkit
from django.db.models import Q

from django.utils.timezone import datetime #important if using timezones


from django.contrib.sites.shortcuts import get_current_site

# Create your views here.
def base(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			return render(request, 'sistema/html/base.html', {'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def template(request,codigo):
		event=Evento.objects.get(codigo=codigo)
		coPersona=Asistencia.objects.get(evento=event.id)
		person=Persona.objects.get(cedula=coPersona.asistente.cedula)
		tipo = "propio"
		if (person.profesional):
			tipo = "profesional"
		elif person.institucion != "UTMACH":
			tipo = "extranjero"
		print  person
		return render(request, "sistema/html/pdfEvento.html", { 'listaeventos': event,'persona': person,'tipo': tipo})

def ordenesPago(request):
		persona = Persona.objects.get(correo=request.user.email)
		listaAsistencia=Asistencia.objects.filter(asistente=persona)
		listaEvento=[]
		for asistencia in listaAsistencia:
			listaEvento.append(
				asistencia.evento
				)
		return render(request, "sistema/html/ordesPagos.html", {'usuarioName': request.user.username, 'lista': listaEvento,
																'activo': persona.activo,'cedula': persona.cedula})

def imprimirEvento(request,codigo):
		event=Evento.objects.get(codigo=codigo)
		print event
		projectUrl = request.get_host() + '/template/'+event.codigo
		pdf = pdfkit.from_url(projectUrl, False)
		# Generate download
		response = HttpResponse(pdf, content_type='application/pdf')
		response['Content-Disposition'] = 'attachment; filename="salidaCer.pdf"'
		return response
		
def imprimirCertificado(request,codigo):
		ass=Asistencia.objects.filter(id=codigo).first()
		projectUrl = request.get_host() + '/generarCertificado/'+str(ass.id)

		print projectUrl
		pdf = pdfkit.from_url(projectUrl, False)
		print projectUrl
		# Generate download
		response = HttpResponse(pdf, content_type='application/pdf')
		response['Content-Disposition'] = 'attachment; filename="salida.pdf"'
		return response

def generarCertificado(request,codigo):
		ass=Asistencia.objects.filter(id=codigo).first()
		evt=ass.evento
		persona=ass.asistente
		print evt.nombre
		
		org  = Organiza.objects.filter(evento=evt)
		lstorg=[]
		for n in org:
			lstorg.append(n.organizador.firma)
		if ass.porcentaje>0.8:
			return render(request, 'sistema/html/certificado.html', {'evento': evt, 'usuarioName': request.user.username,'activo':persona.activo,
																'cedula':persona.cedula,'firmas':lstorg,'estudiante':persona.nombres+ " " + persona.apellidos})
		else:
			return redirect ('/',permanent=False)

def editar_perfil(request, cedula):
		estudiante = Persona.objects.get(cedula=cedula)
		if request.POST:
			form = EstudianteForm(request.POST, instance=estudiante)

			if form.is_valid():
				form.save()
				return redirect('listaEventosU')
		else:
			form = EstudianteForm(instance=estudiante)
			template = 'sistema/html/editarEstudianteU.html'
			persona = Persona.objects.get(correo=request.user.email)
			book = {'form': form, 'usuarioName': request.user.username,'activo':persona.activo,'cedula':persona.cedula}
			return render(request, template, book)

def datoEstudiante(request):

		if request.user.is_authenticated:
			if request.method == "POST":
				print "post"
				form = EstudianteForm(request.POST, request.FILES)
				if form.is_valid():
					estudiante = form.save(commit=False)
					estudiante.usuario = request.user
					estudiante.activo = True
					estudiante.correo = request.user.email
					estudiante.save()
					return redirect('listaEventosU', permanent=False)
				else:
					print form.errors
			else:
					form = EstudianteForm()
			return render(request, 'sistema/html/datosEstudiante.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('listaEventosU', permanent=False)

def baseUsuario(request):
	
		persona = Persona.objects.get(correo=request.user.email)
		return render(request, 'sistema/html/baseUsuario.html', {'usuarioName':request.user.username,'activo':persona.activo,
																 'cedula':persona.cedula})

def listaAsistencia(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Evento.objects.all()
			return render(request, 'sistema/html/listaAsistencias.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def listaValidar(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Asistencia.objects.filter(comprobante="0")
			print lista
			return render(request, 'sistema/html/listaValidar.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def listaEventosU(request):
	
		#query=Evento.objects.all()
			
		if request.POST:
			per = Persona.objects.filter(usuario=request.user).first()
			query = Evento.objects.all()
			for i in query:
				print i.codigo
				if i.codigo in request.POST:
					print per.nombres+" "+per.apellidos+" se inscribio en "+i.nombre
					
					ass=Asistencia(asistente=per,
                                 evento=i,
                                 porcentaje=0,
                                 universidad=per.institucion,
                                 nivel="superior",
                                 profesion="-"
                                 )
					ass.save()
			return redirect('/listaEventosU', permanent=False)
		else:
			today = datetime.today()
			per = Persona.objects.filter(usuario=request.user).first()
			ass= Asistencia.objects.filter(asistente=per)
			lista=[]
			
			for i in ass:
				print i.evento.codigo
				lista.append(i.evento.codigo)
			query=Evento.objects.filter(fin__gte=today, fin__lte=fecha.date(2999, 1, 31)).exclude(codigo__in=lista)#gte es para establecer una fecha inicio, lte para una fecha final

			

			listaeventos = list(query)
			persona = Persona.objects.get(correo=request.user.email)
			print persona.institucion
			print persona.profesional

			if persona.profesional==1:
				tipo = "profesional"
			if str(persona.institucion)== "UTMACH":
				tipo="propio"
			if str(persona.institucion) != "UTMACH":
				tipo = "extranjero"

			return render(request, 'sistema/html/eventosUsuarios.html', {'usuarioName':request.user.username,'listaeventos':listaeventos,'activo':persona.activo,
																		 'cedula':persona.cedula,'tipo':tipo})

def listaEventosAsistidos(request):
	
		#query=Evento.objects.all()
			today = datetime.today()
			per = Persona.objects.filter(usuario=request.user).first()
			ass= Asistencia.objects.filter(asistente=per)
			lista=[]
			
			for i in ass:
				print i.evento.codigo
				lista.append(i.evento.codigo)
			query=Evento.objects.filter(fin__gte=fecha.date(1800, 1, 31), fin__lte=today, codigo__in=lista)#gte es para establecer una fecha inicio, lte para una fecha final
			listaeventos = list(query)
			persona = Persona.objects.get(correo=request.user.email)

			if persona.profesional==1:
				tipo = "profesional"
			if str(persona.institucion)== "UTMACH":
				tipo="propio"
			if str(persona.institucion) != "UTMACH":
				tipo = "extranjero"

			return render(request, 'sistema/html/listEventos.html', {'usuarioName':request.user.username,'listaeventos':listaeventos,'activo':persona.activo,
																		 'cedula':persona.cedula,'tipo':tipo})

def listaEventosPorAsistir(request):
	
		#query=Evento.objects.all()
			
			today = datetime.today()
			per = Persona.objects.filter(usuario=request.user).first()
			ass= Asistencia.objects.filter(asistente=per)
			lista=[]
			
			for i in ass:
				print i.evento.codigo
				lista.append(i.evento.codigo)
			query=Evento.objects.filter(fin__gte=today, fin__lte=fecha.date(2999, 1, 31), codigo__in=lista)#gte es para establecer una fecha inicio, lte para una fecha final
			listaeventos = list(query)
			persona = Persona.objects.get(correo=request.user.email)
			if persona.profesional==1:
				tipo = "profesional"
			if str(persona.institucion)== "UTMACH":
				tipo="propio"
			if str(persona.institucion) != "UTMACH":
				tipo = "extranjero"
			return render(request, 'sistema/html/listEventos.html', {'usuarioName':request.user.username,'listaeventos':listaeventos,'activo':persona.activo,
																		 'cedula':persona.cedula,'tipo':tipo})

def indexUsuario(request):
	#modificar y redireccionar a listareventosu
		query=Evento.objects.all()
		listaeventos = list(query)
		persona = Persona.objects.get(correo=request.user.email)
		return render(request, 'sistema/html/calendar.html', {'usuarioName':request.user.username,'listaeventos':listaeventos,'activo':persona.activo,
															  'cedula':persona.cedula})

def perfilUsuario(request,cedula):
		persona = Persona.objects.get(correo=request.user.email)
		datoU = Persona.objects.get(cedula=cedula)
		return render(request, 'sistema/html/perfil.html', {'datoU': datoU, 'usuarioName': request.user.username,'activo':persona.activo,
															'cedula':persona.cedula})

def verTopico(request,codigo):
		event = Evento.objects.get(codigo=codigo)
		print event.id
		listPonencias = Ponencia.objects.filter(evento=event)
		print listPonencias

		persona = Persona.objects.get(correo=request.user.email)

		return render(request, 'sistema/html/topicos.html', {'evento': event,'lista': listPonencias, 'usuarioName': request.user.username,'activo':persona.activo,
															'cedula':persona.cedula})
        
def login(request):
		
		if request.method=='POST':
			formulario=formLog(request.POST)
			nuevo=newUser(request.POST)
			emailform=email(request.POST)
			if "log" in request.POST and formulario.is_valid:
				print "form"
				usuario=request.POST['username']
				passs=request.POST['password']
				print usuario
				print passs
				#print user.objects.all()
				#print user.objects.filter(username=usuario, password,passs)
				usr = authenticate(username=usuario, password=passs)
				#verificacion=user.objects.filter(username=usuario, password=passs).exists()
				if usr is not None:

					auth.login(request, usr)
					auth_log(request, usr)
					if request.user.is_authenticated:
						print "usuario autenticado"
					base(request)
					full_url = ''.join(['http://', get_current_site(request).domain])
					print full_url
					#path=HttpRequest.get_host()+"ciudad/"
					#Aquí busco el usuario
					existe = Persona.objects.filter(correo=usr.email).exists()
					if existe or usr.is_staff:
						if usr.is_staff:
							return redirect('index',permanent=True)
						else:
							return redirect('listaEventosU', permanent=True)
							
					else:
						return redirect("datoEstudiante")			
						
						#msg.append("login successful")
					#else:
					#	msg.append("disabled account")
				#else:
					#msg.append("invalid login")
				
				#if verificacion==True:
				#	return ciudad(request,usuario)
				#else:
					#return render(request, 'sistema/html/login.html', {'formulariolog':formulario,'nuevo':nuevo})
			if "nuevo" in request.POST and nuevo.is_valid:
				print "nuevo"
				usuario=request.POST['username']
				mail=request.POST['email']
				pas=request.POST['password']
				repass=request.POST['repassword']
				
				print user.objects.all()
				verificacion=user.objects.filter(username=usuario).exists()
				print(verificacion)
				
				if verificacion==False and pas==repass:
					print "holap" + usuario
					usu = user.objects.create_user(username=usuario,
                                 email=mail,
                                 password=pas)
					usu.save()

					usr = authenticate(username=usuario, password=pas)
				#verificacion=user.objects.filter(username=usuario, password=passs).exists()
					if usr is not None:
						print "no es none"					
						print "es activo"
						auth.login(request, usr)
						auth_log(request, usr)
						if request.user.is_authenticated:
							print "usuario autenticado"
						base(request)
						full_url = ''.join(['http://', get_current_site(request).domain])
						print full_url
						#path=HttpRequest.get_host()+"ciudad/"
						return redirect("datoEstudiante")






					return redirect("index")
				else:
					return render(request, 'sistema/html/login.html', {'formulariolog':formulario, 'nuevo':nuevo})
			if "email" in request.POST and emailform.is_valid:
				print "email"#email(request.POST)
				mail=request.POST['retemail']
				print mail+"x"
				val=user.objects.filter(email=mail).exists()
				if val:
					u = user.objects.get(email__exact=mail)
					
										#generar contraseña
					longitud = 18
					valores = u"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"
					p = ""
					p = p.join([choice(valores) for i in range(longitud)])
					print(p)

					u.set_password(p)
					u.save()	
					msg = EmailMessage("prueba", "Su nueva contrasenia es : \n"+p, to=[mail])
					msg.content_subtype = "html"
					msg.send(fail_silently=False)
					print "El correo se ha enviado exitosamente"
				else:
					print "El correo no se ha enviado"
				print "holap"
					
		else:
			formulario=formLog()
			nuevo=newUser()
			emailform=email()
		return render(request, 'sistema/html/login.html', {'formulariolog':formulario, 'nuevo':nuevo,'email':emailform})

def logout(request):
		if request.user.is_authenticated:
			auth_logout(request)
		
		login(request)
		return redirect('/', permanent=False)

def ciudad_nueva(request):
	if request.user.is_staff:	
		if request.user.is_authenticated:
			if request.method == "POST":
				form = CiudadForm(request.POST)
				if form.is_valid():
					post = form.save(commit=False)
					post.save()
					form = CiudadForm()
			else:
					form = CiudadForm()

			return render(request, 'sistema/html/ciudad-form.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)

	else:
		return redirect('/', permanent=False)

def estudiante_nuevo(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			if request.method == "POST":
				print "post"
				form = EstudianteForm(request.POST, request.FILES)
				if form.is_valid():
					estudiante = form.save(commit=False)
					estudiante.usuario = request.user
					estudiante.usuario.is_active = True

					estudiante.save()
					return redirect('/', permanent=False)
				else:
					print form.errors
			else:					
					form = EstudianteForm()
			return render(request, 'sistema/html/estudianteform.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def evento_nuevo(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			errorhoy=False
			errorfechas=False
			agregado=False
			if request.method == "POST":
				ini= datetime.strptime(request.POST.get('inicio'), '%Y-%m-%d')
				form = EventoForm(request.POST, request.FILES)
				if form.is_valid():
					if ini>datetime.today():
						evento = form.save(commit=False)
						evento.fecha_fin()
						if evento.validar_fechas():							
							evento.save()
							form = EventoForm()
							agregado=True
						else:
							errorfechas=True
					else:
						errorhoy=True
				else:
					print "error:form no valida o fecha previa a hoy"
			else:
				form = EventoForm()

			return render(request, 'sistema/html/eventosform.html', {'form': form,'usuarioName':request.user.username,'errorfechas': errorfechas,'errorhoy': errorhoy,'agregado': agregado})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def institucion_nuevo(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			if request.method == "POST":
				form = InstitucionForm(request.POST)
				if form.is_valid():
					post = form.save(commit=False)         
					post.save()
					form = InstitucionForm()
			else:
					form = InstitucionForm()

			return render(request, 'sistema/html/institucionform.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def organizador_nuevo(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			if request.method == "POST":
				form = OrganizadorForm(request.POST, request.FILES)
				if form.is_valid():
					post = form.save(commit=False)                
					post.save()
					form = OrganizadorForm()
			else:
					form = OrganizadorForm()

			return render(request, 'sistema/html/organizadorform.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def organiza_nuevo(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			if request.method == "POST":
				form = OrganizaForm(request.POST)
				if form.is_valid():
					post = form.save(commit=False)                
					post.save()
					form = OrganizaForm()
			else:
					form = OrganizaForm()

			return render(request, 'sistema/html/organizaform.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_ciudades(request): 
	if request.user.is_staff:       
		if request.user.is_authenticated:
			lista = Ciudad.objects.all()
			return render(request, 'sistema/html/listarCiudad.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_organizar(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Organiza.objects.all()
			return render(request, 'sistema/html/listaOrganizar.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_estudiantes(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Persona.objects.all()
			return render(request, 'sistema/html/listarEstudiantes.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_instituciones(request):        
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Institucion.objects.all()
			return render(request, 'sistema/html/listarInstitucion.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_eventos(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Evento.objects.all()
			return render(request, 'sistema/html/listarEventos.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_ponencias(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Ponencia.objects.all()
			return render(request, 'sistema/html/listarPonecias.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def lista_organizador(request):        
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Organizador.objects.all()
			return render(request, 'sistema/html/listarOrganizador.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def editar_ponencias(request, id):
	if request.user.is_staff:
		ponencia = Ponencia.objects.get(id=id)
		if request.POST:
			form=PonenciaForm(request.POST,instance=ponencia)
			print form
			if form.is_valid():
				form.save()
				return redirect('lista_ponencias')
		else:
			form=PonenciaForm(instance=ponencia)
			template = 'sistema/html/editarPonencias.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def editar_ciudad(request, id):
	if request.user.is_staff:	
		ciudad = Ciudad.objects.get(id=id)
		if request.POST:
			form=CiudadForm(request.POST,instance=ciudad)
			print form
			if form.is_valid():
				form.save()
				return redirect('listaCiudad')
		else:
			form=CiudadForm(instance=ciudad)
			template = 'sistema/html/editarCiudad.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def editar_estudiante(request, cedula):
	if request.user.is_staff:
		estudiante = Persona.objects.get(cedula=cedula)
		if request.POST:
			form = EstudianteForm(request.POST, instance=estudiante)

			if form.is_valid():
				form.save()
				return redirect('lista_estudiantes')
		else:
			form = EstudianteForm(instance=estudiante)
			template = 'sistema/html/editarEstudiante.html'
			book = {'form': form, 'usuarioName': request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def editar_organizador(request, cedula):
	if request.user.is_staff:
		organizador = Organizador.objects.get(cedula=cedula)
		if request.POST:
			form=OrganizadorForm(request.POST,instance=organizador)
			print form
			if form.is_valid():
				form.save()
				return redirect('lista_organizador')
		else:
			form=OrganizadorForm(instance=organizador)
			template = 'sistema/html/editarOrganizador.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def editar_institucion(request, id):
	if request.user.is_staff:
		institucion = Institucion.objects.get(id=id)
		if request.POST:
			form=InstitucionForm(request.POST,instance=institucion)
			print form
			if form.is_valid():
				form.save()
				return redirect('lista_instituciones')
		else:
			form=InstitucionForm(instance=institucion)
			template = 'sistema/html/editarInstitucion.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def editar_organiza(request, id):
	if request.user.is_staff:
		organiza = Organiza.objects.get(id=id)
		if request.POST:
			form=OrganizaForm(request.POST,instance=organiza)
			print form
			if form.is_valid():
				form.save()
				return redirect('lista_organizar')
		else:
			form=OrganizaForm(instance=organiza)
			template = 'sistema/html/editaOrganizar.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def editar_evento(request, codigo):
	if request.user.is_staff:
		evento = Evento.objects.get(codigo=codigo)
		print "editar evento"
		if request.POST:
			print "Entre a post"
			form=EventoForm(request.POST,instance=evento)
			if form.is_valid():
				evento = form.save(commit=False)
				print 
				evento.fecha_fin()
				evento.save()
				print "Valido"
				return redirect('lista_eventos')
			else:
				print "Evento no valido"
		else:
			print "no entre a post"
			form=EventoForm(instance=evento)
			template = 'sistema/html/editarEvento.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def validar_asistencia(request, codigo):
	if request.user.is_staff:
		asistencia = Asistencia.objects.get(id=codigo)
		print "editar asistencia"
		if request.POST:
			print "Entre a post"
			form=ValidarAsistenciaForm(request.POST,instance=asistencia)
			if form.is_valid():
				asistencia = form.save(commit=False)
				if asistencia.comprobante!="0":
					asistencia.save()
					print "Valido"
					return redirect('listaValidar')
				else:
					form=ValidarAsistenciaForm(instance=asistencia)
					template = 'sistema/html/validarAsistencia.html'
					book = {'form':form,'usuarioName':request.user.username,'asistencia':asistencia, 'error':True }
					return render(request, template, book)
			else:
				print "asistencia no valida"
				print form.errors
		else:
			print "no entre a post"
			form=ValidarAsistenciaForm(instance=asistencia)
			template = 'sistema/html/validarAsistencia.html'
			book = {'form':form,'usuarioName':request.user.username,'asistencia':asistencia, 'error':False }
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def ponencia_nuevo(request,codigoevento):
	if request.user.is_staff:	
		if request.user.is_authenticated:
			evento = Evento.objects.get(codigo=codigoevento)
			errorfecha=False
			agregado=False
			if request.method == "POST":
				form = PonenciaForm(request.POST)
				if form.is_valid():
					ponencia = form.save(commit=False)					
					if evento.validar_dia(ponencia):
						ponencia.evento=evento
						ponencia.save()
						form = PonenciaForm()
						agregado=True
					else:
						errorfecha=True
				else:
					print form.errors
			else:
					form = PonenciaForm()

			return render(request, 'sistema/html/ponenciaform.html', {'form': form,'usuarioName':request.user.username,'evento':evento,'errorfecha':errorfecha,'agregado':agregado})
		else:
			return redirect('/', permanent=False)

	else:
		return redirect('/', permanent=False)

def horario_nuevo(request,codigoevento):
	if request.user.is_staff:	
		if request.user.is_authenticated:
			evento = Evento.objects.get(codigo=codigoevento)
			errorfecha=False
			agregado=False
			errorhora=False
			errorhoras=False
			if request.method == "POST":
				form = HorarioForm(request.POST)
				if form.is_valid():
					horario = form.save(commit=False)
					if horario.validar_horas():
						if evento.validar_dia(horario):
							if evento.validar_horario(horario):
								horario.evento=evento
								print "dia antes guardado", horario.dia
								horario.save()
								print "dia guardado", horario.dia
								form = HorarioForm()
								agregado = True
							else:
								errorhora=True
						else:
							errorfecha=True
					else:
						errorhoras=True
				else:
					print form.errors
			else:
					form = HorarioForm()

			return render(request, 'sistema/html/horarioform.html', {'form': form,'usuarioName':request.user.username,'evento':evento,'errorfecha':errorfecha,'errorhora':errorhora,'errorhoras':errorhoras,'agregado':agregado})
		else:
			return redirect('/', permanent=False)

	else:
		return redirect('/', permanent=False)

def editar_horario(request, id):
	if request.user.is_staff:
		horario = Horario.objects.get(id=id)
		if request.POST:
			form=OrganizaForm(request.POST,instance=horario)
			print form
			if form.is_valid():
				form.set_horas()
				form.save()
				return redirect('lista_horario')
		else:
			form=HorarioForm(instance=horario)
			template = 'sistema/html/editahorario.html'
			book = {'form':form,'usuarioName':request.user.username}
			return render(request, template, book)
	else:
		return redirect('/', permanent=False)

def lista_horario(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			lista = Horario.objects.all()
			return render(request, 'sistema/html/listaHorario.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def tomar_asistencia(request,idhorario):
	if request.user.is_staff:
		if request.user.is_authenticated:
			horario = Horario.objects.get(id=idhorario)
			if not horario.tomado:
				evento = horario.evento
				listaAsistencias = Asistencia.objects.filter(evento=evento).exclude(comprobante="0")
				print "lista", listaAsistencias
				for asistencia in listaAsistencias:
					RegistroAsistencia.objects.create(asistencia=asistencia,horario=horario,asistio=False,horas=horario.horas)
				horario.tomado = True
				horario.save()

			query = RegistroAsistencia.objects.filter(horario=horario)
			print query

			if request.method == "POST":
				form = TomarAsistenciaForm(request.POST,queryset=[], lleno=True)
				if form.is_valid():
					form.save()
					print "guardado"
					return redirect('listaHorariosPorRegistrar')
				else:
					print form.errors
			else:
				form = TomarAsistenciaForm(queryset=query, lleno=False)

			return render(request, 'sistema/html/asistenciaform.html', {'form': form,'usuarioName':request.user.username})
		else:
			return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)

def listaCertificados(request):

		print "hi"
		if request.POST:
			print "post"
			per = Persona.objects.filter(usuario=request.user).first()
			query = Asistencia.objects.filter(asistente=per)
			lista=[]
			for i in query:
				print i.evento
				x=i.evento
				print x
				evt=Evento.objects.filter(codigo=x.codigo).first()
				horas=evt.horas
				print horas
				hor=Horario.objects.filter(evento=evt)
				reg=RegistroAsistencia.objects.filter(asistencia__in=query,horario__in=hor)
				total=0
				for n in reg:
					total+=n.horas
				porcentaje=total/horas
				i.porcentaje=porcentaje
				i.save()
				if i.porcentaje >  0.8:
					print "generado"
				
					if i in request.POST:
						print "imprimir"#imprimir aqui!
				
				return render(request, 'sistema/html/listaCertificados.html', {'usuarioName':request.user.username,'lista':lista})#impresión

		else:
			print "no post"
			per = Persona.objects.filter(usuario=request.user).first()
			print per
			query = Asistencia.objects.filter(asistente=per)
			print query
			lista=[]
			for i in query:
				x=i.evento
				print "evento"
				print x
				evt=Evento.objects.filter(codigo=x.codigo).first()
				horas=evt.horas
				print "horas"
				print horas
				hor=Horario.objects.filter(evento=evt)
				reg=RegistroAsistencia.objects.filter(asistencia__in=query,horario__in=hor)
				total=0
				for n in reg:
					total+=n.horas
				porcentaje=total/horas
				i.porcentaje=porcentaje
				i.save()
				if i.porcentaje >  0.8:
					print "generado"
					lista.append(i)
			return render(request, 'sistema/html/listaCertificados.html', {'activo':per.activo,'usuarioName':request.user.username,'cedula':per.cedula,'lista':lista})

def listaHorariosPorRegistrar(request):
	if request.user.is_staff:
		if request.user.is_authenticated:
			today = datetime.today()
			query=Horario.objects.all()#gte es para establecer una fecha inicio, lte para una fecha final
			print "Horarios ",query
			lista = list(query)
			return render(request, 'sistema/html/listaHorariosPorRegistrar.html', {'lista': lista,'usuarioName':request.user.username})
		else:
				return redirect('/', permanent=False)
	else:
		return redirect('/', permanent=False)
